import {
  DELETE_FAIL,
  DELETE_SUCCESS,
  DELETE_REQUEST,
  GET_FAIL,
  GET_REQUEST,
  GET_SUCCESS,
  UPLOAD_FAIL,
  UPLOAD_REQUEST,
  UPLOAD_SUCCESS
} from './../constants/actionTypes';

import initialState from './../store/initialState';

const reducer = (state = initialState.photos, action) => {
  let newState = [];
  switch(action.type){
    case DELETE_FAIL:
      // TODO:
      return state;
    case DELETE_SUCCESS:
      newState = [...state].filter(function(photo){
        if(photo.id !== action.data){
          return photo;
        }
      });
      return newState;
    case DELETE_REQUEST:
      // TODO:
      return state;
    case GET_FAIL:
      // TODO:
      return state;
    case GET_REQUEST:
      return initialState.photos;
    case GET_SUCCESS:
      return action.data;
    case UPLOAD_FAIL:
      // TODO:
      return state;
    case UPLOAD_REQUEST:
      // TODO:
      return state;
    case UPLOAD_SUCCESS:
      newState = [...state];
      if(newState.length === 0){
        newState.push(action.data);
      }
      else{
        newState.unshift(action.data);
      }
      return newState;
    default:
      return state;
  }
};

export default reducer;
