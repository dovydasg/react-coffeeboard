import React from 'react';
import FlatPagination from 'material-ui-flat-pagination';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {
  getPhotos,
  deletePhotos
} from "../actions";
import PropTypes from 'prop-types';

import {GridList} from 'material-ui/GridList';

import Form from '../components/Form';
import UploadFile from '../components/UploadFile';
import CustomGridTile from '../components/CustomGridTile';

class HomePage extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      toUpload: [],
      currentPhotos: [],
      pagination: {
        offset: 0,
        limit: 8
      }
    };
  }

  componentWillMount(){
    this.props.actions.getPhotos();
  }

  componentWillReceiveProps(nextProps){
    this.updateCurrentPhotos(nextProps.photos, this.state.pagination.offset);
  }

  onImageAddClick(){
    document.getElementById('filePicker').click();
  }

  onChange(e){
    if(e.target.files && e.target.files.length>0)
      this.onPicturesSelected(e.target.files);
  }

  onPicturesSelected(fileList){
    let title = document.getElementById('title').value;
    let price = document.getElementById('price').value;
    if(title === "" || price === ""){
        alert('Title and price are required');
        return;
    }
    fileList = [...fileList].filter((file)=>{
      file.id = (new Date()).getTime().toString()+file.lastModified.toString();
      file.title = title;
      file.price = price;
      return file;
    });
    this.setState({
      toUpload: [...this.state.toUpload, ...fileList]
    });
  }

  onUploadComplete(fileId) {
    let toUpload = [...this.state.toUpload].filter(function(file){
      if(file.id !== fileId){
        return file;
      }
    });
    this.setState({
      toUpload
    });
  }

  onPictureDelete(photoId){
    this.props.actions.deletePhotos(photoId);
  }

  paginationOffsetChange(offset) {
    this.setState({
      pagination: Object.assign({}, this.state.pagination, {offset: offset})
    });
    this.updateCurrentPhotos(this.props.photos, offset);
  }

  updateCurrentPhotos(photos, offset){
    let currentPhotos;
    currentPhotos = photos.filter((photo, index)=>{
      if(index >= offset && index < offset+this.state.pagination.limit){
        return photo;
      }
    });
    this.setState({
      currentPhotos
    });
  }

  render(){
    return (
      <div>
        <Form onButtonClick={this.onImageAddClick.bind(this)}/>
        <input type="file" name="file" id="filePicker" ref="filePicker" accept="image/png, image/jpeg"
               style={{height: 0, width: 0, visibility: 'hidden'}} multiple={true} onChange={this.onChange.bind(this)}/>
        
        <div className = "main">
          <GridList
            className = "grid-list"
            cols={4}
          >
            {
              this.state.toUpload.map((file, index) => (
                <UploadFile file={file} index={index} key={index} onUploadComplete={this.onUploadComplete.bind(this)}/>
              ))
            }
            {
              this.state.currentPhotos.map((photo, index) => {
                  return <CustomGridTile photo={photo} onPictureDelete={this.onPictureDelete.bind(this)} key={index} index={index} />;
              })
            }
          </GridList>
        </div>
        <FlatPagination
          offset={this.state.pagination.offset}
          limit={this.state.pagination.limit}
          total={this.props.photos.length}
          onClick={(e, offset) => this.paginationOffsetChange(offset)}
          style={{
            textAlign: 'center',
            marginBottom: '40px'
          }}
        />
      </div>
    );
  }

}

HomePage.propTypes = {
  photos: PropTypes.array,
  actions: PropTypes.object
};

function mapStateToProps(state) {
  return {
    photos: state.photos
  };
}

/* Map Actions to Props */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      getPhotos,
      deletePhotos
    }, dispatch)
  };
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(HomePage);

