import React, {Component} from 'react';
import { Button, Label, Input } from 'reactstrap';
import PropTypes from 'prop-types';

class Form extends Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <div className ="create-form-main">
        <div className ="create-form-child">
          <Label for="title">Title</Label>
        </div>
        <div className ="create-form-child">
          <Input type="text" name="title" id="title" placeholder="cofffee title" />
        </div>
        <div className ="create-form-child">
          <Label for="price">Price</Label>
        </div>
        <div className ="create-form-child">
          <Input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" className="form-control currency" id="price" placeholder="cofffee price" />
        </div>
        <div className ="create-form-child">
          <Button color="success" onClick={this.props.onButtonClick} style={{marginBottom: '6px'}}>Upload</Button>
        </div>
      </div>
    );
  }
}

Form.propTypes = {
  onButtonClick: PropTypes.func
};

export default Form;
