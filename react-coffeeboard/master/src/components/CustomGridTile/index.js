import React, {Component} from 'react';
import {GridTile} from 'material-ui/GridList';
import DeleteSVG from 'material-ui/svg-icons/action/delete';
import IconButton from 'material-ui/IconButton';
import PropTypes from 'prop-types';

class CustomGridTile extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <GridTile
        title={"Coffe title: " + this.props.photo.title}
        subtitle={<span>Price: {this.props.photo.price}</span>}
        style={{border: '1px solid black'}}
        actionIcon={<IconButton onClick={()=>{this.props.onPictureDelete(this.props.photo.id);}}><DeleteSVG color="white" /></IconButton>}
        actionPosition="right"
      >
        <img src={this.props.photo.dataUri} />
      </GridTile>
    );
  }
}

CustomGridTile.propTypes = {
  photo: PropTypes.object,
  onPictureDelete: PropTypes.func,
  index: PropTypes.number,
  onClick: PropTypes.func
};


export default CustomGridTile;
