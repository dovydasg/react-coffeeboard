/*
 * Action for feature photos
 * */

import {
  DELETE_SUCCESS,
  DELETE_REQUEST,
  GET_REQUEST,
  GET_SUCCESS,
  UPLOAD_REQUEST,
  UPLOAD_SUCCESS
} from '../constants/actionTypes';

export function getPhotos() {
  return function (dispatch) {
    return new Promise(function(){{
      dispatch({
        type: GET_REQUEST
      });
      
      let photos = localStorage.getItem('coffeeboard');
      if(!photos){
        photos = [];
      }
      else{
        photos = JSON.parse(photos);
        photos = photos.map(function(photo){
          return JSON.parse(photo);
        });
      }

      dispatch({
        type: GET_SUCCESS,
        data: photos
      });

    }});
  };
}

export function uploadPhoto(photo) {
  return function (dispatch) {
    return new Promise(function(){{
      dispatch({
        type: UPLOAD_REQUEST
      });

      let photos = localStorage.getItem('coffeeboard');
      if(!photos){
        photos = [];
        photos.push(JSON.stringify(photo));
      }
      else{
        photos = JSON.parse(photos);
        photos.unshift(JSON.stringify(photo));
      }
      localStorage.setItem('coffeeboard', JSON.stringify(photos));

      dispatch({
        type: UPLOAD_SUCCESS,
        data: photo
      });

    }});
  };
}

export function deletePhotos(photoId) {
  return function (dispatch) {
    return new Promise(function(){{
      dispatch({
        type: DELETE_REQUEST
      });

      let photos = localStorage.getItem('coffeeboard');
      photos = JSON.parse(photos);
      let photosNew = photos.filter(function(photo){
        photo = JSON.parse(photo);
        if(photo.id !== photoId){
          return JSON.stringify(photo);
        }
      });
      localStorage.setItem('coffeeboard', JSON.stringify(photosNew));

      dispatch({
        type: DELETE_SUCCESS,
        data: photoId
      });

    }});
  };
}
