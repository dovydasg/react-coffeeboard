import React from 'react';

import HomePage from './../containers/HomePage';

let routes = [
  {
    path: '/',
    exact: true,
    component: ()=><HomePage/>
  }
];

export default routes;